#!/bin/bash

echo $! > sample-app.pid
sudo kill -9 `cat sample-app.pid`
rm sample-app.pid
